﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type in Three words you'd like displayed on seperate lines");
            var words = Console.ReadLine();
            var output = words.Split(' ');
            Console.WriteLine(output[0]);
            Console.WriteLine(output[1]);
            Console.WriteLine(output[2]);
        }
    }
}
