﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press 1 if you want to convert celcius to fahrenheit");
            Console.WriteLine("Press 2 if you want to convert fahrenheit to celcius");
            var menuSelection = Console.ReadLine();
            switch (menuSelection)
            {
                case "1":
                    Console.WriteLine("You have chosen convert celcius to fahrenheit");                   
                    Console.WriteLine("How many degrees celcius would you like converted to fahrenheit?");
                    var celcius = int.Parse(Console.ReadLine());
                    var convertedtofahrenheit = celcius * 1.8 + 32;
                    Console.WriteLine($"{celcius} degrees celcius in fahrehnheit is {convertedtofahrenheit} degrees fahrenheit");
                    Console.ReadLine();
                    break;

                case "2":
                    Console.WriteLine("You have chosen convert fahrenheit to celcius");                
                    Console.WriteLine("How many degrees fahrenheit would you like converted to celcius?");
                    var fahrenheit = int.Parse(Console.ReadLine());
                    double convertedtocelcius = (fahrenheit - 32) * 5 / 9;
                    Console.WriteLine($"{fahrenheit} degrees fahrenheit in celcius is {convertedtocelcius} degrees celcius");
                    Console.ReadLine();
                    break;
                default:
                    Console.WriteLine("Your selection was invalid");
                    break;
            }
        }
    }
}
