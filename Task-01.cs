﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello What's you're name?");
            var name = Console.ReadLine();
            Console.WriteLine($"And how old are you {name}?");
            var age = Console.ReadLine();
            Console.WriteLine("Your name is " + name + " and you are " + age + " years old");
            Console.WriteLine("Your name is {0} and you are {1} years old", name, age);
            Console.WriteLine($"You name is {name} and you are {age} years old");
        }
    }
}
