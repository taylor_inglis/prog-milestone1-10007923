﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var even = new List<int>();
            var odd = new List<int>();
            var numbers = new int[7] { 33, 45, 21, 44, 67, 87, 86 };
            foreach (var i in numbers)
                if (i % 2 == 0)
                    even.Add(i);
                else
                    odd.Add(i);                              
            odd.ForEach(Console.WriteLine);
        }
    }
}
