﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication16
{
    class Program
    {
        static void Main(string[] args)
        {
            var days = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
            var i = 0;
            var dayOfMonth = 31;
            for (var x = 0; x < dayOfMonth; x++)
            {
                var day = x % 7;
                var dayString = days[day];
                if (dayString == "Wednesday")
                {
                    i++;
                }
            }
            Console.WriteLine($"There are {i} Wednesdays");
        }
    }
}
