﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication8
{
    class Program
    {
        static void Main(string[] args)
        {   Console.WriteLine("Which numbers division table would you like to know?");
            var number = int.Parse(Console.ReadLine());          
            var i = 0;
            var counter = 13;
            for (i = 1; i < counter; i++)
            {
                var divisionnumber = i*number;
                var division = number;
                var printdivision = divisionnumber/division;
                Console.WriteLine($"{divisionnumber}/{division}={printdivision}");
            }
        }
    }
}
