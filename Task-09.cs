﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication14
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;
            var counter = 20;
            for (i = 0; i < counter; i++)
            {
                var year = 2017 + i;
                var leapyear = year;
                leapyear %= 4;
                if (leapyear == 0)
                Console.WriteLine($"{year} is a leapyear");                                                              
            }

        }
    }
}