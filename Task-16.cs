﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication17
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Which word would you like to know the number of letters in?");
            var word = Console.ReadLine();
            char[] wordarray = word.ToCharArray();
            Console.WriteLine($"Your word {word} has {wordarray.Length} letters in it");
        }
    }
}
