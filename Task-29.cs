﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    class Program
    {
        static void Main(string[] args)
        {var words = ("The quick brown fox jumped over the fence");
            var output = words.Split(' ');
            Console.WriteLine($"The sentence {words} has {output.Length} words in it");
        }
    }
}
