﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication7
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;
            var counter = 20;
            for (i = 0; i <= counter; i++)
            {
                if (counter == i)
                {
                    Console.WriteLine($"The counter and index are equal at {counter}"); 
                    
                }
                else
                {
                    Console.WriteLine($"The index was not equal to the counter at {i}");
                }
            }
        }
    }
}
