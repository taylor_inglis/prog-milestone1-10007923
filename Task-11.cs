﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Which year would you like to know if it was a leapyear?");
            var year = int.Parse(Console.ReadLine());
            var leapyear = year;
            leapyear %= 4;
            if (leapyear == 0)
            {
                Console.WriteLine($"The year {year} is a leapyear");
            }    
            else
            {
                Console.WriteLine($"The year {year} is not a leapyear");
            }
        }
    }
}
