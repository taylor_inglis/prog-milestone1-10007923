﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication14
{
    class Program
    {
        static void Main(string[] args)
        {
            var days = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
            var i = 0;
            var dayOfMonth = 31;
            for (var x = 0; x < dayOfMonth; x++)
            {
                var day = x % 7; //worked with Sam and Andrew on line 18 and 19 for Task 39 and 40
                var dayString = days[day]; 
                if (dayString == "Monday")
                {
                    i++;
                }
            }
            Console.WriteLine($"There are {i} Mondays");
        }
     }
}

