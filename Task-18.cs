﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication19
{
    class Program
    {
        static void Main(string[] args)
            {
            var details = new List<Tuple<String, String, String>>()
            {
                Tuple.Create("Steve", "October", "19"),
                Tuple.Create("Jason", "January", "17"),
                Tuple.Create("Sally", "November", "2"),
            };
            details.ForEach(Console.WriteLine);
        }
    }
}
