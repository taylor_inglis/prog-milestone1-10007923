﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            var weekdays = new string[7] {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
            var i = 0;
            var counter = 7;
            for (i = 0; i < counter; i++)
            {
                var weeknumber = i + 1;
                Console.WriteLine($"Day:{weekdays[i]} Day Number:{weeknumber}");
            }
        }
    }
}
