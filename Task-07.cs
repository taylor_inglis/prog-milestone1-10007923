﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Which timetables up to 12 would you like to know?");
            var times = int.Parse(Console.ReadLine());
            var counter = 12;
            var i = 0;
            for (i = 0; i < counter; i++)
            {
                var a = i + 1;
                Console.WriteLine(times * a);
            }
        }
    }
}
